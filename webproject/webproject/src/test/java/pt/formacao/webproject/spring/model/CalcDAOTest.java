package pt.formacao.webproject.spring.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import pt.formacao.webproject.modelo.CalcEntity;
import pt.formacao.webproject.spring.modelo.CalcDAO;

public class CalcDAOTest {

	private static final Logger LOGGER = Logger.getLogger(CalcDAOTest.class);

	private static final int SIZE = 10;
	private static final int ID_1 = 1;
	private static final int ID_10 = 10;
	private static final Integer RESULTADO = 6;


	private static CalcDAO dao;

	@Before
	public void init() {
		if (dao == null) {
			LOGGER.info("Inicializano Teste");
			@SuppressWarnings("resource")
			AbstractApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
			dao = context.getBean("calcDAO", CalcDAO.class);
		}
	}

	@Test
	public void saveTest() {
		for (CalcEntity entity : getListEntity(SIZE)) {			
			dao.setEntity(entity);
			dao.save();
		}
	}

	@Test
	public void updateTest() {
		CalcEntity entity = dao.find(ID_1);
		Assert.assertNotEquals(RESULTADO, entity.getResultado());
		entity.setResultado(RESULTADO);
		dao.setEntity(entity);
		dao.update();
		entity = dao.find(ID_1);
		Assert.assertEquals(RESULTADO, entity.getResultado());
	}

	@Test
	public void removeTest() {
		CalcEntity entity = dao.find(ID_10);
		Assert.assertNotNull(entity);
		dao.setEntity(entity);
		dao.remove();
		entity = dao.find(ID_10);
		Assert.assertNull(entity);
	}

	@Test
	public void find() {
		CalcEntity entity = dao.find(ID_1);
		Assert.assertEquals(ID_1, entity.getId());
	}


	@Test
	public void findByNum1Num2Test() {
		List<CalcEntity> allEntities = dao.findByNum1Num2(null, null);
		Assert.assertEquals(SIZE, allEntities.size());
	}

	@Test
	public void findByResultadoTest() {
		CalcEntity entity = dao.findByResultado(RESULTADO);
		Assert.assertEquals(RESULTADO, entity.getResultado());
	}


	private List<CalcEntity> getListEntity(int size) {
		List<CalcEntity> calcEntities = new ArrayList<CalcEntity>();
		for (int i = 0; i < size; i++) {
			calcEntities.add(getEntity(i, i, i+i));
		}
		return calcEntities;
	}

	private CalcEntity getEntity (int num1, int num2, int resultado) {
		CalcEntity entity = new CalcEntity();
		entity.setNum1(num1);
		entity.setNum2(num2);
		entity.setResultado(resultado);
		return entity;
	}


}
