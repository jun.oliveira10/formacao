package pt.formacao.webproject.spring.modelo;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import pt.formacao.webproject.modelo.CalcEntity;

@Repository
public class CalcDAO extends AbstractHibernateDAO {
	
	private final static Logger LOGGER = Logger.getLogger(CalcDAO.class);
	
	private CalcEntity entity;
	
	public CalcEntity getEntity() {
		return entity;
	}

	public void setEntity(CalcEntity entity) {
		this.entity = entity;
	}
	
	public void save() {
		LOGGER.info("Realizando persistencia da entidade: " + entity);
		Session session = getSessionFactory().openSession();
		try {
			
			session.beginTransaction();
			session.save(entity);
			session.getTransaction().commit();
			LOGGER.info("Entidade Salva: " + entity.getId());
			
		} catch (Exception e) {
			
			LOGGER.error("Erro ao salvar entidade", e);
			session.getTransaction().rollback();
			
		} finally {
			
			session.close();
			
		}
	}
	
	public void update() {
		LOGGER.info("Realizando atualizacao da entidade: " + entity);
		Session session = getSessionFactory().openSession();
		try {
			
			session.beginTransaction();
			session.saveOrUpdate(entity);
			session.getTransaction().commit();
			LOGGER.info("Entidade Atualizada: " + entity.getId());
			
		} catch (Exception e) {
			
			LOGGER.error("Erro ao atualizar entidade", e);
			session.getTransaction().rollback();
			
		} finally {
			
			session.close();
			
		}
	}
	
	public void remove() {
		LOGGER.info("Realizando remocao da entidade: " + entity);
		String hql = "delete CalcEntity where id = :id";
		Session session = getSessionFactory().openSession();
		try {
			
			session.beginTransaction();
			
			Query query = session.createQuery(hql);
			
			query.setInteger("id", entity.getId());
			query.executeUpdate();
		
			session.getTransaction().commit();
			LOGGER.info("Entidade removida: " + entity.getId());
			
		} catch (Exception e) {
			
			LOGGER.error("Erro ao atualizar entidade", e);
			session.getTransaction().rollback();
			
		} finally {
			
			session.close();
			
		}
	}
	
	public CalcEntity find(int id) {
		LOGGER.info("Buscando entidade por id: " + id);
		
		String queryStr = "select c from CalcEntity c where c.id = :id";
		
		Session session = getSessionFactory().openSession();
		Query query = session.createQuery(queryStr);
		query.setInteger("id", id);
		entity = (CalcEntity) query.uniqueResult();
		return entity;

	}
	
	@SuppressWarnings("unchecked")
	public List<CalcEntity> findByNum1Num2(Integer num1, Integer num2) {
		LOGGER.info("Buscando entidade numero 1 ou numero 2: numero 1 " + num1 + " numero 2 " + num2);
		
		StringBuilder queryStr = new StringBuilder("select * from CALCULADORA where 1=1 ");
		
		if(num1 != null) {
			queryStr.append(" and num1 = :num1");
		}
		
		if(num2 != null) {
			queryStr.append(" and num1 = :num2");
		}
		
		Session session = getSessionFactory().openSession();
		Query query = session.createSQLQuery(queryStr.toString());
		
		if(num1 != null) {
			query.setInteger("num1", num1);
		}
		
		if(num2 != null) {
			query.setInteger("num2", num2);
		}
	
		return (List<CalcEntity>) query.list();

	}
	
	public CalcEntity findByResultado(int resultado) {
		LOGGER.info("Buscando entidade por resultado: " + resultado);
		Session session = getSessionFactory().openSession();
		Query query = session.getNamedQuery(CalcEntity.QUERY_BY_RESULTADO);
		query.setInteger("resultado", resultado);
		entity = (CalcEntity) query.uniqueResult();
		return entity;
	}

}
