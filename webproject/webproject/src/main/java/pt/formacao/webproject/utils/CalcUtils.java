package pt.formacao.webproject.utils;

public class CalcUtils {
		
	private static CalcUtils instance;
	
	private CalcUtils() {
		
	}
	
	public static CalcUtils getInstance() {
		if (instance == null ) {
			instance = new CalcUtils();
		}
		return instance;
	}
	
	
	public int soma(int num1, int num2) {
		return num1 + num2;
	}
	
	public int subtracao(int num1, int num2) {
		return num1 - num2;
	}
	
	public int divisao(int num1, int num2) {
		return num1 / num2;
	}
	
	public int multiplicacao(int num1, int num2) {
		return num1 * num2;
	}

}
