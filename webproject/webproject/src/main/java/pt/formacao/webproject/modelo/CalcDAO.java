package pt.formacao.webproject.modelo;

import javax.ejb.Singleton;

import org.apache.log4j.Logger;

@Singleton
public class CalcDAO {
	
	private final static Logger LOGGER = Logger.getLogger(CalcDAO.class);
	
	private CalcEntity entity;

	public CalcEntity getEntity() {
		return entity;
	}

	public void setEntity(CalcEntity entity) {
		this.entity = entity;
	}
	
	public void save() {
		LOGGER.info("Entidade Salva: " + entity);
	}
	
	public void update() {
		LOGGER.info("Entidade atualizada: " + entity);
	}
	
	public void remove() {
		LOGGER.info("Entidade removida: " + entity);
	}
	
	public CalcEntity find(int id) {
		LOGGER.info("Entidade encontrada: " + id);
		return entity;
	}

}
