package pt.formacao.webproject.visao;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.log4j.Logger;

import pt.formacao.webproject.controle.CalcEJB;
import pt.formacao.webproject.dto.CalcDTO;

@Named("calcManagedBean")
@ViewScoped
public class CalcManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(CalcManagedBean.class);

	private static final int ESTADO_INICIAL = 1;
	private static final int ESTADO_RESULTADO = 2;
	private static int ESTADO_ATUAL;
	

	@EJB
	private CalcEJB calcEJB;

	private CalcDTO calcDTO;
	
	private String operacao;
	
	@PostConstruct
	public void init() {
		calcDTO = new CalcDTO();
		operacao = "soma";
		ESTADO_ATUAL = ESTADO_INICIAL;
	}
	
	public String getOperacao() {
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}

	public CalcDTO getCalcDTO() {
		return calcDTO;
	}

	public void setCalcDTO(CalcDTO calcDTO) {
		this.calcDTO = calcDTO;
	}
	
	public boolean isEstadoInicial() {
		return ESTADO_ATUAL == ESTADO_INICIAL;
	}
	
	public boolean isEstadoResultado() {
		return ESTADO_ATUAL == ESTADO_RESULTADO;
	}

	public void calcula() {
		LOGGER.error("Opreção executada " + operacao);
		try {
			switch (operacao) {
			case "soma":
				calcEJB.soma(calcDTO);
				break;
			case "subtracao":
				calcEJB.subtracao(calcDTO);
				break;
			case "divisao":
				calcEJB.divisao(calcDTO);
				break;
			case "multiplicacao":
				calcEJB.multiplicacao(calcDTO);
				break;
				
			default:
				throw new IllegalArgumentException();
			}
			ESTADO_ATUAL = ESTADO_RESULTADO;
		}catch (Exception e) {
			LOGGER.error("Erro ao executar a operação " + operacao, e);
		}
	}

}
