package pt.formacao.webproject.spring.beans;

public interface Shape {

	public void draw();
	
}
