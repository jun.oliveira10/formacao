package pt.formacao.webproject.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "CALCULADORA")
@NamedQuery(name = CalcEntity.QUERY_BY_RESULTADO, query = "select c from CalcEntity c where c.resultado = :resultado")
public class CalcEntity {
	
	public static final String QUERY_BY_RESULTADO = "calcEntity.byResultado";
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name = "NUMERO_1")
	private Integer num1;
	
	@Column(name = "NUMERO_2")
	private Integer num2;
	
	@Column(name = "RESULTADO")
	private Integer resultado;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getNum1() {
		return num1;
	}

	public void setNum1(Integer num1) {
		this.num1 = num1;
	}

	public Integer getNum2() {
		return num2;
	}

	public void setNum2(Integer num2) {
		this.num2 = num2;
	}

	public Integer getResultado() {
		return resultado;
	}

	public void setResultado(Integer resultado) {
		this.resultado = resultado;
	}

}
