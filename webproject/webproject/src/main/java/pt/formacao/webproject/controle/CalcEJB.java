package pt.formacao.webproject.controle;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;

import org.apache.commons.lang3.math.NumberUtils;

import pt.formacao.webproject.builder.CalcBuilder;
import pt.formacao.webproject.dto.CalcDTO;
import pt.formacao.webproject.modelo.CalcDAO;
import pt.formacao.webproject.modelo.CalcEntity;
import pt.formacao.webproject.utils.CalcUtils;

@Singleton
public class CalcEJB {

	@EJB
	private CalcDAO dao;
	
	private CalcUtils calcUtils;
	
	@PostConstruct
	public void init() {
		calcUtils = CalcUtils.getInstance();
	}
	
	public void soma(CalcDTO calcDTO) throws Exception {
		if(validaRegraSoma(calcDTO)) {
			int num1 = calcDTO.getNum1();
			int num2 = calcDTO.getNum2();
			int resultado = calcUtils.soma(num1, num2);
			calcDTO.setResultado(resultado);
			CalcEntity entity = CalcBuilder.toEntity(calcDTO);
			dao.setEntity(entity);
			dao.save();
		} else {
			throw new Exception();
		}
	}
	
	public void subtracao(CalcDTO calcDTO) throws Exception {
		if(validaRegraSubtracao(calcDTO)) {
			int num1 = calcDTO.getNum1();
			int num2 = calcDTO.getNum2();
			int resultado = calcUtils.subtracao(num1, num2);
			calcDTO.setResultado(resultado);
			CalcEntity entity = CalcBuilder.toEntity(calcDTO);
			dao.setEntity(entity);
			dao.save();
		} else {
			throw new Exception();
		}
	}
	
	public void multiplicacao(CalcDTO calcDTO) throws Exception {
		if(validaRegraMultiplicacao(calcDTO)) {
			int num1 = calcDTO.getNum1();
			int num2 = calcDTO.getNum2();
			int resultado = calcUtils.multiplicacao(num1, num2);
			calcDTO.setResultado(resultado);
			CalcEntity entity = CalcBuilder.toEntity(calcDTO);
			dao.setEntity(entity);
			dao.save();
		} else {
			throw new Exception();
		}
	}
	
	public void divisao(CalcDTO calcDTO) throws Exception {
		if(validaRegraDivisao(calcDTO)) {
			int num1 = calcDTO.getNum1();
			int num2 = calcDTO.getNum2();
			int resultado = calcUtils.divisao(num1, num2);
			calcDTO.setResultado(resultado);
			CalcEntity entity = CalcBuilder.toEntity(calcDTO);
			dao.setEntity(entity);
			dao.save();
		} else {
			throw new Exception();
		}
	}
	
	private boolean validaRegraGerais(CalcDTO calcDTO) {
		return calcDTO.getNum1() != null && calcDTO.getNum2() != null;
	}

	private boolean validaRegraSoma(CalcDTO calcDTO) {
		return validaRegraGerais(calcDTO);
	}
	
	private boolean validaRegraSubtracao(CalcDTO calcDTO) {
		return validaRegraGerais(calcDTO);
	}
	
	private boolean validaRegraMultiplicacao(CalcDTO calcDTO) {
		return validaRegraGerais(calcDTO);
	}
	
	private boolean validaRegraDivisao(CalcDTO calcDTO) {
		return validaRegraGerais(calcDTO) 
				&& ( calcDTO.getNum1() > NumberUtils.INTEGER_ZERO 
						&& calcDTO.getNum2() > NumberUtils.INTEGER_ZERO );
	}
	
	
}
