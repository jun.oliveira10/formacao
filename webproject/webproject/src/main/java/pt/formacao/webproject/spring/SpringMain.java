package pt.formacao.webproject.spring;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import pt.formacao.webproject.modelo.CalcEntity;
import pt.formacao.webproject.spring.modelo.CalcDAO;

public class SpringMain {

	public static void main(String[] args) {
		
		// Implentation of Application contex interface who provides lifecyle
		AbstractApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		// Called to call Destroy method
		context.registerShutdownHook();
		
		CalcEntity entity = new CalcEntity();
		entity.setNum1(1);
		entity.setNum2(2);
		entity.setResultado(3);
		
		CalcDAO dao = context.getBean("calcDAO", CalcDAO.class);		
		dao.setEntity(entity);
		dao.save();
		
		CalcEntity entityByResultado = null;
		
		entityByResultado = dao.findByResultado(3);
		
		System.out.println(entityByResultado.toString());
		
//		Shape shape = (Shape) context.getBean("circle");		
//		shape.draw();
		
//		System.out.println(context.getMessage("message.example", new Object[] {"Value of the parameter"}, "Default message", null));
		
	}
	
}
