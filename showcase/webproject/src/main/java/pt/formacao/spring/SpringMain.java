package pt.formacao.spring;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import pt.formacao.pojo.Shape;

public class SpringMain {

	public static void main(String[] args) {
		
		// Implentation of Application contex interface who provides lifecyle
		AbstractApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		// Called to call Destroy method
		context.registerShutdownHook();
		Shape shape = (Shape) context.getBean("circle");		
		shape.draw();
		
//		System.out.println(context.getMessage("message.example", new Object[] {"Value of the parameter"}, "Default message", null));
		
	}

}
