package pt.formacao.pojo;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.MessageSource;

public class Circle implements Shape {

	private Point center;
	
	@Autowired
	private MessageSource messageSource;


	public Point getCenter() {
		return center;
	}

	//	@Required
	//	@Autowired
	//	@Qualifier("center")
	@Resource(name="pointC")
	public void setCenter(Point center) {
		this.center = center;
	}
	
	

	public MessageSource getMessageSource() {
		return messageSource;
	}

	// instead of @Autowired
//	@Resource(name="messageSource")
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public void draw() {
		System.out.println("pointA X = " + getCenter().getX() + " PointA Y = " + getCenter().getY());
		System.out.println(messageSource.getMessage("message.example", new Object[] {"Value of the parameter"}, "Default message", null));
	}
	
	

}
