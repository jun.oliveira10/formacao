package pt.formacao.pojo;

public interface Shape {

	public void draw();
	
}
