package pt.formacao.pojo;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class Triangle implements InitializingBean, DisposableBean, Shape {
	
	private Point pointA;
	private Point pointB;
	private Point pointC;
	
	public Point getPointA() {
		return pointA;
	}
	public void setPointA(Point pointA) {
		this.pointA = pointA;
	}
	public Point getPointB() {
		return pointB;
	}
	public void setPointB(Point pointB) {
		this.pointB = pointB;
	}
	public Point getPointC() {
		return pointC;
	}
	public void setPointC(Point pointC) {
		this.pointC = pointC;
	}
	
	public void draw() {
		System.out.println("pointA X = " + getPointA().getX() + " PointA Y = " + getPointA().getY());
		System.out.println("pointB X = " + getPointB().getX() + " PointB Y = " + getPointB().getY());
		System.out.println("pointC X = " + getPointC().getX() + " PointC Y = " + getPointC().getY());
	}
	
	// could be used PosConstruct Annotation
	public void init() {
		System.out.println("Init has called");
	}
	
	// could be used PreDestroy Annotation
	public void customDestroy() {
		System.out.println("customDestroy has called");
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("afterPropertiesSet has called");
	}
	@Override
	public void destroy() throws Exception {
		System.out.println("Destroy has called");
	}

}
