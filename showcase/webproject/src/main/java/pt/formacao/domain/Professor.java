package pt.formacao.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PROFESSOR")
public class Professor {
	
	@Id
	@GeneratedValue
	@Column(name="PROFESSOR_ID")
	private int id;
	
	@Column(name="NOME")
	private String nome;
	
	@Column(name="DATA_INCLUSAO")
	private Date dataInclusao;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataInclusao() {
		return dataInclusao;
	}

	public void setDataInclusao(Date dataInclusao) {
		this.dataInclusao = dataInclusao;
	}

}
