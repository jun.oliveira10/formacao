package pt.formacao.domain;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="DISCIPLINA")
public class Disciplina {
	
	@Id
	@GeneratedValue
	@Column(name="DISCIPLINA_ID")
	private int id;
	
	@Column(name="NOME")
	private String nome;
	
	@ManyToMany(cascade = CascadeType.ALL, mappedBy = "listDisciplina")
	private Collection<Turma> listDisciplina =  new ArrayList<Turma>();
	
	
	public Collection<Turma> getListDisciplina() {
		return listDisciplina;
	}

	public void setListDisciplina(Collection<Turma> listDisciplina) {
		this.listDisciplina = listDisciplina;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
