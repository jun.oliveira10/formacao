package pt.formacao.domain;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="TURMA")
public class Turma {
	
	@Id
	@GeneratedValue
	@Column(name="TURMA_ID")
	private int id;
	
	private String nome;
	
	@JoinTable(name="TURMA_PROFESSOR", joinColumns = @JoinColumn(name="PROFESSOR_ID"))
	@OneToOne(cascade = CascadeType.ALL)
	private Professor professor;
	
	@OneToMany(cascade = CascadeType.ALL)
	private Collection<Aluno> listAluno =  new ArrayList<Aluno>();
	
	@ManyToMany(cascade = CascadeType.ALL)
	private Collection<Disciplina> listDisciplina =  new ArrayList<Disciplina>();

	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Collection<Disciplina> getListDisciplina() {
		return listDisciplina;
	}

	public void setListDisciplina(Collection<Disciplina> listDisciplina) {
		this.listDisciplina = listDisciplina;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}

	public Collection<Aluno> getListAluno() {
		return listAluno;
	}

	public void setListAluno(Collection<Aluno> listAluno) {
		this.listAluno = listAluno;
	}
	
}
