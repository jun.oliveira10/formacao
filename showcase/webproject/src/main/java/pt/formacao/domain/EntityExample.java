package pt.formacao.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="ENTITY_EXAMPLE")
public class EntityExample implements Serializable{
	
	private static final long serialVersionUID = 2434348311802534716L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long ID;
	
	private String name;
	
	private Integer age;
	
	@Temporal(TemporalType.DATE)
	private Date birthDate; 
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	
	
}
