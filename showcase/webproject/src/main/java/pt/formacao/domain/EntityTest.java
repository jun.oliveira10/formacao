package pt.formacao.domain;

import java.util.ArrayList;
import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class EntityTest {
	
	
	private static org.hibernate.SessionFactory sessionFactory;

	  public static SessionFactory getSessionFactory() {
	    if (sessionFactory == null) {
	      initSessionFactory();
	    }
	    return sessionFactory;
	  }

	  private static synchronized void initSessionFactory() {
	    sessionFactory = new Configuration().configure().buildSessionFactory();

	  }

	  public static Session getSession() {
	    return getSessionFactory().openSession();
	  }

	
	
	public static void main(String[] args) {
		
//		EntityExample entity = new EntityExample();
//		entity.setName("Name 1");
//		entity.setBirthDate(new Date());
//		entity.setCreationDate(new Date());
//		entity.setAge(15);
		
		ArrayList<Aluno> listAluno = getListAluno("Aluno", 3);
		ArrayList<Disciplina> listDisciplina = getListDisciplina("Disciplina", 3);
		Professor professor = getProfessor("Professor");
		Turma turma = new Turma();
		turma.setNome("Turma");
		turma.setListAluno(listAluno);
		turma.setListDisciplina(listDisciplina);
		turma.setProfessor(professor);
		
		SessionFactory sessionFactory = getSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(turma);
		session.getTransaction().commit();
		
//		
//		turma = null;
//		
//		turma = (Turma) session.get(Turma.class, 1);
		
		
		session.close();
		
	}
	
	
	private static ArrayList<Disciplina> getListDisciplina(String nome, int quantidade) {
		ArrayList<Disciplina> list = new ArrayList<Disciplina>();
		
		for(int i=0; i<quantidade+1 ; i++) {
			list.add(getDisciplina(nome + i));
		}
		
		return list;
	}
	
	private static Disciplina getDisciplina(String nome) {
		Disciplina disciplina = new Disciplina();
		disciplina.setNome(nome);
		return disciplina;
	}
	
	
	private static ArrayList<Aluno> getListAluno(String nome, int quantidade) {
		ArrayList<Aluno> list = new ArrayList<Aluno>();
		
		for(int i=0; i<quantidade+1 ; i++) {
			list.add(getAluno(nome + i));
		}
		
		return list;
	}
	
	private static Aluno getAluno(String nome) {
		Aluno aluno = new Aluno();
		aluno.setNome(nome);
		aluno.setDataInclusao(new Date());
		return aluno;
	}
	
	private static Professor getProfessor(String nome) {
		Professor professor = new Professor();
		professor.setNome(nome);
		professor.setDataInclusao(new Date());
		return professor;
	}
	
	

}
