package pt.formacao.jsf;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.apache.log4j.Logger;

import pt.formacao.pojo.Employee;

@Named("managedBeanExample")
@ViewScoped
public class ManagedBeanExample implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger.getLogger(ManagedBeanExample.class);
	private List<Employee> employeeList = new ArrayList<>();
	private Random Random = new Random();
	private boolean state = true;
	private String console;

	@PostConstruct
	private void postConstruct () {
		for (int i = 1; i < 20; i++) {
			Employee employee = new Employee();
			employee.setId(i);
			employee.setName("Teste " + i);
			employee.setPhoneNumber(String.format("%s-%s-%s", Random.nextInt(),
					Random.nextInt(),
					Random.nextInt()));
			employee.setAddress(Random.nextInt() + ", City");
			employeeList.add(employee);
		}
	}

	public String getConsole() {
		return console;
	}

	public void setConsole(String console) {
		this.console = console;
	}

	public List<Employee> getEmployeeList () {
		return employeeList;
	}

	public boolean rendered() {
		return state;
	}

	public String toggleState() {
		state = !state;
		return null;
	}

}