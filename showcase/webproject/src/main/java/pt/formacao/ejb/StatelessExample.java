package pt.formacao.ejb;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

@Stateless
public class StatelessExample {

	private static final Logger LOG = Logger.getLogger(StatelessExample.class);

	@PostConstruct
	public void init() {
		LOG.info(getClass().getSimpleName() + " Created.");
		LOG.info(Thread.currentThread().getName() + " Printed.");
	}
	
	public void print() {
		LOG.info(Thread.currentThread().getId() + " Printed.");
	}

}
