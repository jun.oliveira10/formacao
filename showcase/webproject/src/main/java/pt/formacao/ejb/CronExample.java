package pt.formacao.ejb;

import javax.ejb.Schedule;
import javax.ejb.Schedules;
import javax.ejb.Singleton;

import org.apache.log4j.Logger;

@Singleton
public class CronExample {

	private static final Logger LOG = Logger.getLogger(CronExample.class);

	@Schedules({
		@Schedule(month = "9", dayOfMonth = "20-Last", minute = "0", hour = "8"),
		@Schedule(month = "10", dayOfMonth = "1-10", minute = "0", hour = "8")
	})
	private void cronScheduled() {
		LOG.info(getClass().getSimpleName() + " Scheduled Cron");
	}

	@Schedule(second = "*", minute = "*", hour = "*")
	private void cronPerSecond() {
		LOG.info(getClass().getSimpleName() + " Cron executed every second: second = \"*\", minute = \"*\", hour = \"*\"");
	}
	
	@Schedule(second = "0", minute = "*", hour = "*")
	private void cronPerMinute() {
		LOG.info(getClass().getSimpleName() + " Cron executed every minute: second = \"0\", minute = \"*\", hour = \"*\"");
	}
	
	@Schedule(second = "0", minute = "0", hour = "*")
	private void cronPerHour() {
		LOG.info(getClass().getSimpleName() + " Cron executed every hour: second = \"0\", minute = \"0\", hour = \"*\"");
	}

}
