package pt.formacao.ejb;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.apache.log4j.Logger;

@Startup
@Singleton
public class StartupExample {

	private static final Logger LOG = Logger.getLogger(StartupExample.class);

	@PostConstruct
	public void init() {
		LOG.info(getClass().getSimpleName() + " Created.");
		LOG.info(Thread.currentThread().getName() + " Printed.");
	}
	
	public void print () {
		LOG.info(Thread.currentThread().getName() + " Printed.");
	}

}
