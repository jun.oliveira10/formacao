package pt.formacao.springboot.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface CalcRepository  extends CrudRepository<CalcEntity, Integer>{
	
	
	@Query(value = "select c from CalcEntity c where c.resultado = :resultado")
	CalcEntity findByResultado(@Param("resultado") Integer resultado);

	@Query(value = "select c from CalcEntity c where c.num1 = :num1")
	CalcEntity findByNum1(@Param("num1") Integer num1);
	
	@Query(value = "select * from calculadora c where c.NUMERO_2 = :num2", nativeQuery = true)
	CalcEntity findByNum2(@Param("num2") Integer num2);
	
}
