package pt.formacao.springboot.dto;

import org.springframework.stereotype.Component;

@Component
public class CalcDTO {
	
	private int id;
	private Integer num1;
	private Integer num2;
	private Integer resultado;
	
	public CalcDTO() {
		
	}
	
	
	public CalcDTO(int id, Integer num1, Integer num2, Integer resultado) {
		this.id = id;
		this.num1 = num1;
		this.num2 = num2;
		this.resultado = resultado;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getNum1() {
		return num1;
	}
	
	public void setNum1(Integer num1) {
		this.num1 = num1;
	}
	
	public Integer getNum2() {
		return num2;
	}
	
	public void setNum2(Integer num2) {
		this.num2 = num2;
	}
	
	public Integer getResultado() {
		return resultado;
	}
	
	public void setResultado(Integer resultado) {
		this.resultado = resultado;
	}
	
}
