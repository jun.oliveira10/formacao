package pt.formacao.springboot.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.formacao.springboot.builder.CalcBuilder;
import pt.formacao.springboot.dto.CalcDTO;
import pt.formacao.springboot.repository.CalcEntity;
import pt.formacao.springboot.repository.CalcRepository;

@Service
public class CalcService {

	private static final Logger LOGGER = Logger.getLogger(CalcService.class);
	
	@Autowired
	private CalcRepository calcRepository;

	private List<CalcDTO> list =  new ArrayList<>(Arrays.asList(
			new CalcDTO(1 ,1, 1, 2),
			new CalcDTO(2, 2, 2, 4),
			new CalcDTO(3, 3, 3, 6),
			new CalcDTO(4, 4, 4, 8),
			new CalcDTO(5, 5, 5, 10)
			));

	@PostConstruct
	public void init() {
		LOGGER.info("init called");
		list.stream()
		.map(CalcBuilder::toEntity)
		.forEach(calcRepository::save);
	}

	public List<CalcDTO> getAllCalcDTO() {
		LOGGER.info("getAllCalcDTO called");
		List<CalcDTO> dtoList = new ArrayList<CalcDTO>();
		calcRepository.findAll()
		.forEach(entity -> dtoList.add(CalcBuilder.toDTO(entity)));
		return dtoList;
	}

	public CalcDTO getById(int id) {
		LOGGER.info("getAllCalcDTO called");
		return calcRepository.findById(id)
				.map(CalcBuilder::toDTO)
				.get();
//		return list.stream()
//				.filter(dto -> dto.getId() == id)
//				.findFirst()
//				.get();
	}

	public CalcDTO getByResultado(int resultado) {
		LOGGER.info("getByResultado called");
		return CalcBuilder.toDTO(calcRepository.findByResultado(resultado));
//		return list.stream()
//				.filter(dto -> dto.getResultado() == resultado)
//				.findFirst()
//				.get();
	}
	
	public CalcDTO getByNum1Num2(Integer num1, Integer num2) {
		LOGGER.info("getByNum1Num2 called");
		if(num1 != null) {
			return CalcBuilder.toDTO(calcRepository.findByNum1(num1));
		} else if (num2 != null) {
			return CalcBuilder.toDTO(calcRepository.findByNum2(num2));
		} else {
			return null;
		}
	}

	public void add(CalcDTO dto) {
		LOGGER.info("add called");
		calcRepository.save(CalcBuilder.toEntity(dto));
	}

	public CalcDTO update(int id, CalcDTO dto) {
		LOGGER.info("update called");
		CalcEntity entity = CalcBuilder.toEntity(dto);
		entity.setId(id);
		calcRepository.save(entity);
//		CalcDTO calcDTO = list.stream()
//				.filter(d -> d.getId() == id)
//				.findFirst()
//				.orElse(null);
//		if (calcDTO != null) {
//			list.set(list.indexOf(calcDTO), dto);
//			return dto;
//		}
		return dto;
	}

	public boolean delete(int id) {
		LOGGER.info("delete called");
		calcRepository.delete(CalcBuilder.toEntity(getById(id)));
		return true;
//		boolean idExists = list.stream().anyMatch(dto -> dto.getId() == id);
//		if(idExists) {
//			list.stream()
//			.filter(d -> d.getId() != id)
//			.collect(Collectors.toList());
//			return true;
//		}
//		return false;		
		
//		return list.removeIf(d -> d.getId() != id);
	}


}
