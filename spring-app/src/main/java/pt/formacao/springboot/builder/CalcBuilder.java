package pt.formacao.springboot.builder;

import pt.formacao.springboot.dto.CalcDTO;
import pt.formacao.springboot.repository.CalcEntity;

public class CalcBuilder {
	
	private CalcBuilder() {
		
	}
	
	public static CalcDTO toDTO(CalcEntity entity) {
			CalcDTO dto = new CalcDTO();
			dto.setId(entity.getId());
			dto.setNum1(entity.getNum1());
			dto.setNum2(entity.getNum2());
			dto.setResultado(entity.getResultado());
			return dto;
	}
	
	public static CalcEntity toEntity(CalcDTO dto) {
		CalcEntity entity = new CalcEntity();
//		entity.setId(dto.getId());
		entity.setNum1(dto.getNum1());
		entity.setNum2(dto.getNum2());
		entity.setResultado(dto.getResultado());
		return entity;
	}

}
