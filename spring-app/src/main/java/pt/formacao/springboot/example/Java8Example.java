package pt.formacao.springboot.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Java8Example {
	
	static List<Integer> list = new ArrayList<>(Arrays.asList(new Integer[] {9,5,1,4,7,2,3,5,7,1,1,4,7,5,2,6,5,7}));
	static final Integer FILTRO = 6;
	
	
	public static void main(String[] args) {
		javaAntigo();
		javaNovo();
		
	}
	
	
	private static void javaNovo() {
		
		List<Integer> lista = list.stream()
							.filter(num1 -> num1 >= FILTRO)
							.sorted()
							.collect(Collectors.toList());
		
//		lista.sort(Comparator.naturalOrder());
		
		lista.forEach(System.out::print);
		
		
	}
	
	private static void javaAntigo() {
		List<Integer> listaFiltrada = filtraLista(FILTRO);
		ordenaLista(listaFiltrada);
		
		for (Integer valor : listaFiltrada) {
			System.out.println(valor);
		}
		
	}
	
	private static List<Integer> filtraLista(Integer filtro) {
		List<Integer> listaFiltrada = new ArrayList<Integer>();
		
		for(Integer valor : list) {
			if(valor >= filtro) {
				listaFiltrada.add(valor);
			}
		}
		
		return listaFiltrada;
		
	}
	
	private static void ordenaLista(List<Integer> listaParaOrdenar) {
		listaParaOrdenar.sort(new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				return o1.compareTo(o2);
			}
		});
	}
	

}
