package pt.formacao.springboot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pt.formacao.springboot.dto.CalcDTO;
import pt.formacao.springboot.service.CalcService;

@RestController
@RequestMapping("/calc")
public class CalcController {
	
	@Autowired
	private CalcService calcService;

	@RequestMapping("/all")
	public List<CalcDTO> getAll() {
		return calcService.getAllCalcDTO();
	}
	
	@RequestMapping("/{id}")
	public CalcDTO getById(@PathVariable int id) {
		return calcService.getById(id);
	}
	
	@GetMapping("/resultado")
	public CalcDTO getByResultado(@RequestParam(required = true) int resultado) {
		return calcService.getByResultado(resultado);
	}
	
	@GetMapping("/num")
	public CalcDTO getByNum1Num2(@RequestParam(required = false) Integer num1, @RequestParam(required = false) Integer num2) {
		return calcService.getByNum1Num2(num1, num2);
	}
	
	@RequestMapping(method = RequestMethod.POST, value="/add")
	public void addCalcDTO(@RequestBody CalcDTO dto) {
		calcService.add(dto);
	}
	
	
	@RequestMapping(method = RequestMethod.PUT, value="/update/{id}")
	public CalcDTO updateCalcDTO(@PathVariable int id, @RequestBody CalcDTO dto) {
		return calcService.update(id, dto);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/delete/{id}")
	public boolean delete(@PathVariable int id) {
		return calcService.delete(id);
	}
	
}
